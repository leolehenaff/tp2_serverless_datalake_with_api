# Créer un bucket aws_s3_bucket

resource "aws_s3_bucket" "b" {
  bucket        = "s3-nalia-datasets"
  acl           = "private"
  force_destroy = true
}

# Créer un répertoire aws_s3_bucket_object 

resource "aws_s3_bucket_object" "datasets" {
  bucket = aws_s3_bucket.b.id
  acl    = "private"
  key    = "datasets/brut/"
  source = "/dev/null"
}

# Créer un event aws_s3_bucket_notification pour trigger la lambda

resource "aws_s3_bucket_notification" "bucket_notification" {
  bucket = aws_s3_bucket.b.id

  lambda_function {
    lambda_function_arn = "${aws_lambda_function.data_processing.arn}"
    events              = ["s3:ObjectCreated:Put"]
    filter_prefix       = "datasets/brut/"
    filter_suffix       = ".csv"
  }
}